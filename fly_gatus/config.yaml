debug: false
metrics: false

storage:
  type: sqlite
  path: /persistent/gatus.db
  caching: false # enable when number of monitored endpoints grows larger

ui:
  title: "RDB status"
  header: "RDB infrastructure health status"
  description: "Live monitoring of RDB infrastructure health status"
  logo: "/public/logo.svg"
  link: ""
  buttons:
    - name: "Home"
      link: "https://rdb.vote/"
    - name: "GitLab"
      link: "https://gitlab.com/zdaarau/webapps/rdb/"

# doc: https://github.com/TwiN/gatus#client-configuration
client:
  timeout: 60s

# doc: https://github.com/TwiN/gatus#alerting
alerting:
  email:
    from: "status@rdb.vote"
    username: "${SMTP_USERNAME}"
    password: "${SMTP_PASSWORD}"
    host: "smtp.mailbox.org"
    port: 465
    to: "info@rdb.vote"
    default-alert:
      enabled: true
      description: "RDB health check failed"
      send-on-resolved: true
      failure-threshold: 5
      success-threshold: 5
  gitlab:
    webhook-url: "https://gitlab.com/zdaarau/webapps/rdb/alerts/notify/gatus/f7c78e7907c4a723.json"
    authorization-key: "${GITLAB_WEBHOOK_KEY}"
    default-alert:
      enabled: true
      description: "RDB health check failed"
      send-on-resolved: true
      failure-threshold: 5
      success-threshold: 5

# doc: https://github.com/TwiN/gatus#maintenance
maintenance:
  enabled: false
  timezone: "Europe/Zurich"
  start: "01:00"
  duration: "2h"
  every: []

# doc: https://github.com/TwiN/gatus#connectivity
connectivity:
  checker:
    target: 1.1.1.1:53
    interval: 60s

# doc: https://github.com/TwiN/gatus#configuration
# see also: https://gatus.io/docs/conditions
endpoints:
  #
  # Static sites
  #
  #  - name: "Landing Page"
  #    group: "Frontend"
  #    enabled: true
  #    url: "https://rdb.vote"
  #    interval: 60s
  #    conditions:
  #      - "[STATUS] == 200"
  #      - "[CERTIFICATE_EXPIRATION] > 12h"
  #    ui:
  #      badge:
  #        response-time:
  #          thresholds: [60, 100, 150, 250, 500]
  #    alerts:
  #      - type: email
  #        description: "Health check failed for RDB Landing Page (https://rdb.vote)"
  - name: "Report"
    group: "Static sites"
    enabled: true
    url: "https://report.rdb.vote"
    interval: 60s
    conditions:
      - "[STATUS] == 200"
      - "[CERTIFICATE_EXPIRATION] > 12h"
    ui:
      badge:
        response-time:
          thresholds: [60, 100, 150, 250, 500]
    alerts:
      - type: email
        description: "Health check failed for RDB Report (https://report.rdb.vote)"
  - name: "API Documentation (Swagger UI)"
    group: "Static sites"
    enabled: true
    url: "https://api-doc.rdb.vote"
    interval: 60s
    conditions:
      - "[STATUS] == 200"
      - "[CERTIFICATE_EXPIRATION] > 12h"
    ui:
      badge:
        response-time:
          thresholds: [60, 100, 150, 250, 500]
    alerts:
      - type: email
        description: "Health check failed for RDB API Documentation (https://api-doc.rdb.vote)"
  - name: "GraphQL IDE (GraphiQL)"
    group: "Static sites"
    enabled: true
    url: "https://graphiql.rdb.vote"
    interval: 300s
    conditions:
      - "[STATUS] == 200"
      - "[CERTIFICATE_EXPIRATION] > 12h"
    ui:
      badge:
        response-time:
          thresholds: [60, 100, 150, 250, 500]
    alerts:
      - type: email
        description: "Health check failed for RDB GraphQL IDE (https://graphiql.rdb.vote)"
  #
  # Backend
  #
  - name: "PostgreSQL read-only replica (Neon.tech)"
    group: "Backend"
    enabled: true
    url: "tcp://ep-frosty-flower-a28keh10.eu-central-1.aws.neon.tech:5432"
    interval: 60s
    conditions:
      - "[CONNECTED] == true"
    ui:
      badge:
        response-time:
          thresholds: [30, 60, 100, 250, 500]
      hide-hostname: true
    alerts:
      - type: email
        description: "Health check failed for RDB PostgreSQL (Neon.tech)"
  - name: "OpenAPI description (PostgREST)"
    group: "Backend"
    enabled: true
    url: "https://api.rdb.vote"
    interval: 300s
    conditions:
      - "[STATUS] == 200"
      - "[CERTIFICATE_EXPIRATION] > 12h"
      - '[BODY].info.title == RDB API'
    ui:
      badge:
        response-time:
          thresholds: [100, 150, 250, 500, 750]
    alerts:
      - type: email
        description: "Health check failed for RDB OpenAPI description (https://api.rdb.vote)"
      - type: gitlab
        description: "Health check failed for RDB OpenAPI description (`fly_postgrest`)"
  - name: "API (PostgREST): GET /referendum_types"
    group: "Backend"
    enabled: true
    url: "https://api.rdb.vote/referendum_types?limit=1"
    interval: 15m
    conditions:
      - "[STATUS] == 200"
    ui:
      badge:
        response-time:
          thresholds: [60, 100, 150, 250, 500]
    alerts:
      - type: email
        description: "Health check failed for API: GET /referendum_types (https://api.rdb.vote/referendum_types)"
      - type: gitlab
        description: "Health check failed for API: GET /referendum_types (`fly_postgrest`)"
  - name: "API (PostgREST): GET /referendums"
    group: "Backend"
    enabled: true
    url: "https://api.rdb.vote/referendums?limit=1"
    interval: 15m
    conditions:
      - "[STATUS] == 200"
    ui:
      badge:
        response-time:
          thresholds: [60, 100, 150, 250, 500]
    alerts:
      - type: email
        description: "Health check failed for API: GET /referendums (https://api.rdb.vote/referendums)"
      - type: gitlab
        description: "Health check failed for API: GET /referendums (`fly_postgrest`)"
  - name: "API (PostgREST): GET /rpc/graphql"
    group: "Backend"
    enabled: true
    url: "https://api.rdb.vote/rpc/graphql?query=%7B%20__schema%20%7B%20queryType%20%7B%20name%20%7D%20%7D%20%7D"
    interval: 15m
    conditions:
      - "[STATUS] == 200"
      - '[BODY].data.__schema.queryType.name == Query'
    ui:
      badge:
        response-time:
          thresholds: [60, 100, 150, 250, 500]
    alerts:
      - type: email
        description: "Health check failed for API: GET /rpc/graphql (https://api.rdb.vote/rpc/graphql)"
      - type: gitlab
        description: "Health check failed for API: GET /rpc/graphql (`fly_postgrest`)"
  #
  # Internal tools
  #
  - name: "Admin (NocoDB)"
    group: "Internal tools"
    enabled: true
    url: "https://admin.rdb.vote"
    interval: 60s
    conditions:
      - "[STATUS] == 200"
      - "[CERTIFICATE_EXPIRATION] > 12h"
    ui:
      badge:
        response-time:
          thresholds: [60, 100, 150, 250, 500]
    alerts:
      - type: email
        description: "Health check failed for RDB NocoDB (https://admin.rdb.vote)"
      - type: gitlab
        description: "Health check failed for RDB NocoDB (`fly_nocodb`)"
  - name: "RStudio Server"
    group: "Internal tools"
    enabled: true
    url: "https://rstudio.rdb.vote/auth-sign-in"
    interval: 24h
    conditions:
      - "[STATUS] == 200"
      - "[CERTIFICATE_EXPIRATION] > 12h"
    ui:
      badge:
        response-time:
          thresholds: [2500, 4000, 8000, 15000, 30000]
    alerts:
      - type: email
        description: "Health check failed for RDB RStudio Server (https://rstudio.rdb.vote/auth-sign-in)"
      - type: gitlab
        description: "Health check failed for RDB RStudio Server (`fly_rstudio-server`)"
