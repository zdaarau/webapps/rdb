# RDB PostgREST

[![Health](https://status.rdb.vote/api/v1/endpoints/backend_api-(postgrest):-get--referendums/health/badge.svg)](https://status.rdb.vote/endpoints/backend_api-(postgrest):-get--referendums)
[![Uptime (30 days)](https://status.rdb.vote/api/v1/endpoints/backend_api-(postgrest):-get--referendums/uptimes/30d/badge.svg)](https://status.rdb.vote/endpoints/backend_api-(postgrest):-get--referendums)
[![Response time (30 days)](https://status.rdb.vote/api/v1/endpoints/backend_api-(postgrest):-get--referendums/response-times/30d/badge.svg)](https://status.rdb.vote/endpoints/backend_api-(postgrest):-get--referendums)

RDB's public [RESTful](https://en.wikipedia.org/wiki/REST) API is provided by a [PostgREST](https://postgrest.org/) server hosted on [Fly](https://fly.io/) that
connects to our Neon [read replica](https://neon.tech/docs/introduction/read-replicas) `ep-frosty-flower-a28keh10`. This API is served on
[**`api.rdb.vote`**](https://api.rdb.vote/) and is currently read-only[^1].

PostgREST generates a full [OpenAPI](https://en.wikipedia.org/wiki/OpenAPI_Specification)[^2] description from which [statically served API
documentation](https://gitlab.com/zdaarau/websites/api-doc.rdb.vote) accessible under [**`api-doc.rdb.vote`**](https://api-doc.rdb.vote/) is generated.

PostgREST has a built-in connection pool that is set to the maximum number of simultaneous connections our read replica can handle (\~450). If the number of
simultaneous API users exceeds that maximum, requests are put in a waiting queue and time out if no connection slot becomes available within 10 seconds[^3].
Should it prove necessary in the future to serve more than 500 users simultaneously (which is rather unlikely), we could increase the Neon read replica's upper
[compute size limit](https://neon.tech/docs/manage/endpoints#compute-size-and-autoscaling-configuration) or enable Neon's [connection
pooling](https://neon.tech/docs/connect/connection-pooling#enable-connection-pooling) and configure PostgREST accordingly[^4].

This repository contains the non-sensitive PostgREST configuration. Sensitive configuration data is stored via [Fly
secrets](https://fly.io/docs/reference/secrets/). Currently this includes the following [PostgREST environment
variables](https://postgrest.org/en/stable/references/configuration.html#environment-variables):

-   `PGRST_DB_URI`

[^1]: Consequently, PostgREST's [authentication capabilities](https://postgrest.org/en/stable/references/auth.html) are not used, the
    [`jwt-*`](https://postgrest.org/en/stable/references/configuration.html#jwt-aud) configuration options are left unset and the API methods to *write* data
    (`POST`, `DELETE`, `PATCH`) do not work. Unfortunately, the `POST`, `DELETE` and `PATCH` endpoints cannot be hidden from the documentation until
    [PostgREST/postgrest#1870](https://github.com/PostgREST/postgrest/issues/1870) is fixed.

[^2]: An OpenAPI description is standardized structured metadata about an API that other tools can use to generate code, documentation, test cases, etc. for
    that API. It frees (external) developers from repetitive and error-prone programming tasks and makes an API more accessible.

    The OpenAPI description PostgREST generates currently adheres to version 2.0 of the specification (called *Swagger* in the past). An upgrade to version 3 is
    tracked in [this issue](https://github.com/PostgREST/postgrest/issues/932). See also [this issue](https://github.com/PostgREST/postgrest/issues/1698) and
    the [PostgREST/postgrest-openapi](https://github.com/PostgREST/postgrest-openapi) repository.

[^3]: 10 seconds is the default of the corresponding
    [`db-pool-acquisition-timeout`](https://postgrest.org/en/stable/references/configuration.html#db-pool-acquisition-timeout) configuration option, which of
    course could be increased. However, note that increasing connection pool size and/or timeout should only be used as a last resort [after trying to increase
    the performance of the database itself](https://postgrest.org/en/stable/references/connection_pool.html#acquisition-timeout).

[^4]: I.e. [set](https://postgrest.org/en/stable/references/connection_pool.html#using-external-connection-poolers) the following environment variables:

    ``` toml
    [env]
    PGRST_DB_CHANNEL_ENABLED = "false"
    PGRST_DB_POOL = "9999"
    PGRST_DB_PREPARED_STATEMENTS = "false"
    ```

## Hosting details

The Fly [app](https://fly.io/docs/reference/apps/) is named `rdb-postgrest`.

Currently, we run only a single [`shared-cpu-1x` instance with *256 MB* RAM](https://fly.io/docs/about/pricing/#compute) hosted in the [*Paris, France*
region](https://fly.io/docs/reference/regions/). Should it prove necessary to expand performance for our main users (located in Switzerland), we could [increase
RAM](https://fly.io/docs/flyctl/scale-memory/) and/or [switch to a faster CPU](https://fly.io/docs/flyctl/scale-vm/) anytime[^5].

Should we also want to provide fast access for non-European users, we could [run multiple instances](https://fly.io/docs/apps/scale-count/) of PostgREST [in
multiple regions](https://fly.io/docs/apps/scale-count/#scale-an-app-s-regions).

[^5]: Note that scaling automatically restarts the app. The most relevant documentation on (auto)scaling Fly apps includes:

    -   [Scale Machine CPU and RAM](https://fly.io/docs/apps/scale-machine/)
    -   [Scale the Number of Machines](https://fly.io/docs/apps/scale-count/)
    -   [Automatically Stop and Start Machines](https://fly.io/docs/apps/autostart-stop/)

## Admin access

Currently we use a container image derived from the [official minimal image](https://github.com/PostgREST/postgrest/tree/main/nix/tools/docker) which doesn't
even include a shell (to reduce attack surface). This means we can't connect into the root filesystem of the `rdb-postgrest` Fly app via an [SSH
tunnel](https://fly.io/docs/flyctl/ssh-console/). If this should prove necessary for some reason[^6] in the future, we could base our image e.g. on [Alpine
Linux](https://en.wikipedia.org/wiki/Alpine_Linux).

[^6]: Currently, we need to restart the Fly app (`flyctl apps restart`) to [reload PostgREST's schema
    cache](https://postgrest.org/en/stable/references/schema_cache.html#schema-reloading). With working SSH access on the other hand, we could reload the schema
    cache on-the-fly via:

    ``` sh
    flyctl ssh console --command 'killall -SIGUSR1 postgrest'
    ```

## Notes

-   PostgREST can be automatically notified on schema changes via [suitable PostgreSQL functions and event
    triggers](https://postgrest.org/en/stable/references/schema_cache.html#automatic-schema-cache-reloading). But since [only PostgreSQL superusers can create
    event triggers](https://www.postgresql.org/docs/current/sql-createeventtrigger.html), we [can't do that on
    Neon](https://discord.com/channels/1176467419317940276/1216517379366846474).

    To manually notify PostgREST of schema changes, we can execute the SQL command `NOTIFY pgrst, 'reload schema';` as long as PostgREST is configured to
    `LISTEN` on the `pgrst` channel. Since [connections to PostgreSQL read replicas can't `LISTEN` on
    channels](https://github.com/PostgREST/postgrest/issues/2781), PostgREST must be given the host for the read-write Neon instance as second `host`
    [connection parameter](https://postgrest.org/en/latest/references/configuration.html#db-uri) value, which [it then uses exclusively to `LISTEN` on the
    `pgrst` channel](https://github.com/PostgREST/postgrest/pull/3462). Major downside of this approach is that `LISTEN`ing on a PostgreSQL server keeps it
    active, i.e. the server cannot reasonably auto-suspend (after suspension, it is immediately waken up again by PostgREST which tries to re-establish the lost
    `LISTEN` connection). Therefore, we disabled `LISTEN`ing on the `pgrst` channel (by setting `PGRST_DB_CHANNEL_ENABLED = "false"`).

    Since our PostgREST container image [doesn't allow SSH connections](#admin-access), we can't use the alternative of [sending a `killall -SIGUSR1 postgrest`
    signal](https://postgrest.org/en/stable/references/schema_cache.html#schema-cache-reloading) to our instance.

    This leaves us with the only remaining alternative to trigger a PostgREST schema cache refresh: Restart or re-deploy the Fly app!

-   We could set the [`admin-server-port`](https://postgrest.org/en/stable/references/configuration.html#admin-server-port) (which must differ from the regular
    `server-port`) for PostgREST's [health check endpoints](https://postgrest.org/en/stable/references/admin.html#health-check) and then use the lighter-weight
    one (`/live`) to set a [HTTP service check](https://fly.io/docs/reference/configuration/#services-http_checks) instead of the current TCP one in `fly.toml`.
    But since the current TCP check is even faster and checks basically the same as PostgREST's `/live` endpoint, we stick with the current configuration.

-   The sorting of the endpoints in the OpenAPI description PostgREST generates is not yet configurable. Once the [corresponding
    issue](https://github.com/PostgREST/postgrest/issues/2872) is fixed, we should revisit!
