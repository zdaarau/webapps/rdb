# TODOs

-   Submit issue about hiding the write API endpoints in the OpenAPI description if
    [`openapi-security-active`](https://postgrest.org/en/latest/references/configuration.html#openapi-security-active)
    ([needed](https://github.com/PostgREST/postgrest/issues/1082#issuecomment-1184914791) for Swagger UI's <kbd>Authorize</kbd> button) is disabled. This would
    require to also enable `openapi-security-active` *by default* since otherwise the default OpenAPI output would change (which would be a breaking change).

    Further investigate whether this wasn't already discussed. Maybe implementing [JWT verification without role
    change](https://github.com/PostgREST/postgrest/issues/3002) would be a prerequisite?

-   Currently, the amount of metadata Fly Proxy can handle (which includes set HTTP response headers) [is limited to 512
    Bytes](https://community.fly.io/t/setting-http-response-headers-on-fly-proxy-breaks-deployment/17248/2). Exceeding that limit silently breaks deployment
    (app never becomes reachable). Thus we commented out certain headers in `fly.toml` for now which we should re-enable once [this issue is
    fixed](https://community.fly.io/t/setting-http-response-headers-on-fly-proxy-breaks-deployment/17248/5).

    To test the headers returned by a URL, run: `curl -sS -D - https://api.rdb.vote/ -o /dev/null`

-   Once the RDB [has a logo](https://gitlab.com/zdaarau/meta/rdb/-/issues/29), create a `favicon.ico` file, include it in Dockerfile and serve it via Fly
    [statics](https://fly.io/docs/reference/configuration/#the-statics-sections)

-   Add [`pg-notify-stdout`](https://github.com/mkleczek/pg-notify-stdout) to our container image to compensate for the currently missing automatic schema cache
    reloading. 

-   Once PostGREST [supports storing its schema cache permanently](https://github.com/PostgREST/postgrest/discussions/2918), we could

    -   scale the Fly app to zero (would increase initial API response times but save money&energy).
    -   scale the accessed Neon compute instance to zero (would also save money&energy). Currently it's waked up by PostgREST almost immediately (after
        `FATAL: terminating connection due to administrator command`, which seems to refer to the Postgres auto-suspend).

-   It would probably be a good idea to implement some kind of rate limiting to safeguard our read replica against DoS attacks; PostgREST's documentation only
    mentions [how to set up nginx for this](https://postgrest.org/en/v12/explanations/nginx.html#rate-limiting), but more modern reverse proxies like
    [caddy](https://caddyserver.com/docs/modules/http.handlers.rate_limit#github.com/mholt/caddy-ratelimit) support the same.
