# TODOs

## schema/infrastructure

-   finish adapting R package `rdb` to our new PostgreSQL DB

    -   finish PGSQL data backup -\> restoral in rdb R pkg; debug from

        ``` r
        rdb::reset_rdb(origin_nocodb = "https://admin-testing.rdb.vote",
                       hostname_pg = "ep-rough-bread-a2i616p5.eu-central-1.aws.neon.tech",
                       reset_db = TRUE,
                       keep_data = TRUE)
        ```

    -   implement `rdb::add_attachment()` (wrapper around [`nocodb::upload_attachments()`](https://nocodb.rpkg.dev/reference/upload_attachments))

    -   migrate old data to new schema

        -   for a start, excl. the old `inst_*` fields
        -   see [`schema_overhaul.md`](https://gitlab.com/zdaarau/meta/rdb/-/blob/main/schema_overhaul.md?ref_type=heads) for a mapping between old and new
            fields

    -   implement clean-up of table `nocodb_users` (remove rows whose `nocodb_id`s don't occur in any of the current `created_by`/`updated_by` cols)

    -   adapt remaining functions like the ones for [data augmentation](https://pg--rdb-rpkg-dev.netlify.app/dev/reference/#augmentation) and
        [transformation](https://pg--rdb-rpkg-dev.netlify.app/dev/reference/#transformation) and
        [visualizations](https://pg--rdb-rpkg-dev.netlify.app/dev/reference/#visualization) to new data/schema (some of them could be replaced by convenience
        wrappers around dm, I guess; others like `unnest_var()` are likely obsolete)

-   finish PGSQL schema

    -   add missing `referendum_types` columns; see [`schema_overhaul.md`](https://gitlab.com/zdaarau/meta/rdb/-/blob/main/schema_overhaul.md?ref_type=heads)
    -   add another trigger function to ensure that `referendums.id_official` is filled for non-draft rows if the corresponding `administrative_unit_id` has
        `administrative_units.guarantees_referendum_id_official = TRUE` set, i.e. is known to assign an official ID to referendums
    -   auto-fill `referendum_types.administrative_unit_id` with the lowest-level administrative unit of all (transitively) linked legal instruments (from
        junction table `referendum_types_legal_norms.legal_norm_id` -\> `legal_norms.legal_instrument_display` -\> `legal_instruments.administrative_unit_id`);
        this would simplify adding new referendum types and implies updating/changing the `check_referendum_types_legal_norms_administrative_units` trigger
        function
    -   try to add `BEFORE DELETE` triggers that inhibits user `nocodb` from deleting non-draft entries in `referendums` and `referendum_types`
    -   try to define `referendums.attachments` col as `jsonb` and `administrative_units.level` and `legal_instruments.hierarchy_level` as ENUMs and create
        trigger functions to return them as `text` `WHEN (CURRENT_USER = 'nocodb')` (it will likely fail, but it's worth a try)

-   add metadata ("codebook") to DB:

    -   as sep table for "single source of truth", maybe populated via R fn
    -   then from this table
        -   create SQL [`COMMENT`](https://www.postgresql.org/docs/current/sql-comment.html)s on DB objects [for
            PostgREST](https://postgrest.org/en/stable/references/api/openapi.html)
        -   via `nocodb::update_tbl_col(description = "...")` for NocoDB [field
            descriptions](https://docs.nocodb.com/fields/actions-on-field/#add--edit-field-description)\
            open question: is rich-text/markup/markdown supported?

-   finish automating NocoDB UI config

    -   test new ["Postgres Data Reflection"](https://docs.nocodb.com/getting-started/self-hosted/environment-variables/#postgres-data-reflection) (only used
        when `NC_DB` is set to a PG connection string, IIUC)
    -   finish `config_nocodb_tbl_cols()`
        -   column titles
        -   column order
        -   column visibility
    -   column types
    -   create `legal_instruments` Kanban view for `hierarchy_level` via API
    -   `referendums` tbl has two virtual M2M cols `languages` and `languages1` which shouldn't be there? likely transitively from `referendum_titles` and
        `referendum_questions`... create minimal reprex and submit bug report!

-   implement `legal_norms.clause` white-space normalization (ideally directly in PGSQL)

-   implement URL archiving on archive.org, either in rdb R pkg or directly in PGSQL if possible

-   add indices to DB to optimize performance. [citation](https://github.com/formbricks/formbricks/pull/1593#issuecomment-1798768762) from [Review Agent Prime
    Bot](https://github.com/apps/review-agent-prime):

    > Adding indexes to the database can significantly improve the performance of read operations. However, it's important to note that while indexes speed up
    > read operations, they slow down write operations (such as `INSERT` and `UPDATE`) because the database needs to update the index every time a record is
    > added or modified. Therefore, it's crucial to find a balance and **only add indexes to columns that are frequently used in `WHERE` clauses or `JOIN`
    > operations**.

    also from [Postgres doc](https://www.postgresql.org/docs/current/ddl-constraints.html#DDL-CONSTRAINTS-FK):

    > Since a `DELETE` of a row from the referenced table or an `UPDATE` of a referenced column will require a scan of the referencing table for rows matching
    > the old value, it is often a good idea to index the referencing columns too. Because this is not always needed, and there are many choices available on
    > how to index, the declaration of a foreign key constraint does not automatically create an index on the referencing columns.

    the [pg_stat_statements](https://neon.tech/docs/extensions/pg_stat_statements) extension might also prove useful...

-   upgrade to [PostgreSQL 17](https://www.postgresql.org/about/news/postgresql-17-released-2936/) (Neon announcement is
    [here](https://neon.tech/blog/postgres-17)); to upgrade, we need to recreate the Neon project (which shouldn't be too much of a pain thanks to
    `rdb::reset_rdb()`)

## data

-   add missing attachments from SWITCH Drive folder
    [`/RDB/200_data/220_data_to_import/`](https://drive.switch.ch/index.php/apps/files/?dir=/RDB/200_data/220_data_to_import&fileid=6799599121)

    see [`zdaarau/rpkgs/rdb/stash/aargau.R`](https://gitlab.com/zdaarau/rpkgs/rdb/-/blob/pg/stash/aargau.R?ref_type=heads) for an R code snippet that translates
    the German referendum title to English via the DeepL API

-   overhaul `topics`

    -   make topic `homosexuals` somewhat broader, e.g. `sexual orientation / gender identity`.
    -   shorten topic `compensation for loss of earnings for persons on military service or civil protection duty`?

-   check out VoteInfo data

    -   Schnittstellenstandard Voteinfo [eCH-0252](https://www.ech.ch/de/ech/ech-0252/1.0.0)

-   explore and add Wikidata IDs to all relevant tables

    -   theoretically, it's possible to directly access Wikidata RDF data from PostgreSQL via the [`rdf_fdw`
        extension](https://github.com/jimjonesbr/rdf_fdw?tab=readme-ov-file#wikidata); unfortunately, [Neon.tech doesn't support that extension
        (yet)](https://neon.tech/docs/extensions/pg-extensions)

-   try to augment referendum data with campaign budgets from [das Geld + die Politik](https://moneyinpolitics.ch/)

## analysis/programming

-   create reprex and submit bug report about `DBI::dbAppendTable()` & Co. misinterpreting dates \< 0100-01-01 for PostgreSQL

-   create Hugo website

    -   based on a suitable Hugo theme/module, possibly ported to Hugopress
    -   create pages for DB entries dynamically at build-time from API data
        -   great tutorial on how to create sites from (RESTful API) data in Hugo \< v0.126.0 is found
            [here](https://www.thenewdynamic.com/article/toward-using-a-headless-cms-with-hugo-part-2-building-from-remote-api/)
        -   Hugo v0.126.0 brought [content adapters](https://gohugo.io/content-management/content-adapters/)! see also [this
            issue](https://github.com/gohugoio/hugo/issues/12427) for the origination
        -   the [`hugo-metrics-parser`](https://discourse.gohugo.io/t/a-cli-tool-to-display-intuitive-hugo-build-performance/50313) CLI seems useful for large
            sites, try it out!

-   check out [Bytebase](https://github.com/nocodb/nocodb/issues/9098#issuecomment-2254685886)

-   try out [NocoBase](https://docs.nocobase.com/welcome/getting-started/installation)

    -   demo instance can be created [here](https://demo.nocobase.com/new)
    -   see the [handbook](https://docs.nocobase.com/handbook) for usage instructions

## other

-   start to [sponsor NocoDB via GitHub](https://github.com/sponsors/nocodb), ideally with the \$199/month option, which gives us "feature prioritization", so
    our requests for further development should be given priority, and see [how it goes](https://github.com/nocodb/nocodb/issues/5592#issuecomment-1711684274);
    we can suspend the sponsorship anytime

-   test whether NocoDB actually [adheres](https://docs.nocodb.com/data-sources/connect-to-data-source/) to the `?sslmode=verify-full` [connection
    mode](https://neon.tech/docs/connect/connect-securely)

    -   it might be necessary to first [set the `PGSSLROOTCERT` env var](https://neon.tech/docs/connect/connect-securely#connect-from-the-psql-client)
    -   for NocoDB specifically, see also the
        [`knownQueryParams`](https://github.com/nocodb/nocodb/blob/master/packages/nocodb/src/utils/nc-config/constants.ts#L39-L76) const for supported params
        in connection strings
    -   try setting [`ssl: { rejectUnauthorized: true }`](https://nodejs.org/api/tls.html#tls_tls_connect_options_callback) in `nocodb::create_data_src()` as
        that is what the underlying knex.js [seems to expect](https://github.com/brianc/node-postgres/issues/2089#issuecomment-580266063); see also [this
        comment](https://github.com/nocodb/nocodb/issues/9098#issuecomment-2254685886)

## outsourced (contract work?)

-   **NocoDB** development

    If not already done, get accustomed with [NocoDB](https://nocodb.com/) and their [contribution guidelines](https://github.com/nocodb/nocodb/issues/601) and
    sign [their Contributor License Agreement](https://cla-assistant.io/nocodb/nocodb).

    Then thoroughly comprehend [our setup](https://gitlab.com/zdaarau/webapps/rdb/-/tree/main/fly_nocodb) (NocoDB with litestreamed SQLite for internal
    metadata, PostgreSQL as external data source for RDB data and Backblaze B2 bucket for file attachments).

    Then your main task is to help fix the NocoDB issues and implement the features [that are relevant to
    us](https://gitlab.com/zdaarau/webapps/rdb/-/blob/main/fly_nocodb/TODO.md?ref_type=heads#open-issues).

    WE DO NOT WANT A CUSTOM FORK OF NOCODB! Instead, we want to have all your improvements upstreamed to NocoDB, so you will have to work with the NocoDB dev
    team and follow their advice (e.g. change requests on PRs). We strongly recommend to discuss implementation details beforehand with NocoDB team members
    ([@dstala](https://github.com/dstala) coordinates NocoDB development).
