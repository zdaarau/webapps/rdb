# RDB

This repository contains code and configuration related to the **R**eferendum **D**ata**b**ase (RDB) that aims to record all direct democratic votings worldwide
and is operated by the [Centre for Democracy Studies Aarau (ZDA)](https://www.zdaarau.ch/) at the [University of Zurich](https://www.uzh.ch/), Switzerland.

## Overview

-   [**`fly_backup/`**](fly_backup) contains the configuration for our database backup app hosted on [Fly](https://fly.io/) which saves a weekly snapshot of the
    whole RDB PostgreSQL database to two different object storage services.

-   [**`fly_gatus/`**](fly_gatus) contains the configuration for our [Gatus](https://gatus.io/) server hosted on Fly, which provides a status page for all the
    RDB services, applications and endpoints that are expected to provide uninterrupted operation.

-   [**`fly_nocodb/`**](fly_nocodb) contains the configuration for our [NocoDB](https://www.nocodb.com/) server hosted on Fly, which provides the front-end for
    our data coders to edit RDB entries.

-   [**`fly_postgrest/`**](fly_postgrest) contains the configuration for our [PostgREST](https://postgrest.org/)[^1] server hosted on Fly that turns the heart
    of the RDB -- our PostgreSQL database -- into a high-quality [RESTful](https://en.wikipedia.org/wiki/REST) API including a full
    [OpenAPI](https://en.wikipedia.org/wiki/OpenAPI_Specification) description, from which [auto-generated API
    documentation](https://gitlab.com/zdaarau/websites/api-doc.rdb.vote) is offered under [**`api-doc.rdb.vote`**](https://api-doc.rdb.vote/).

    Additionally, this RESTful API's [`/rpc/graphql` endpoint](https://api-doc.rdb.vote/#/(rpc)%20graphql) exposes a full [GraphQL](https://graphql.org/) API
    provided by the PostgreSQL extension [pg_graphql](https://supabase.github.io/pg_graphql/), which is [designed to interoperate with
    PostgREST](https://supabase.com/docs/guides/database/extensions/pg_graphql). As an in-browser GraphQL IDE, a preconfigured
    [GraphiQL](https://github.com/graphql/graphiql/tree/main/packages/graphiql#readme) instance is offered under
    [**`graphiql.rdb.vote`**](https://graphiql.rdb.vote/).

    Since we do all database writes directly via PostgreSQL, PostgREST is connected to our Neon read replica, so both APIs are read-only and do not allow to
    alter the database.

-   [**`fly_rstudio-server/`**](fly_rstudio-server) contains the configuration for our preconfigured [RStudio
    Server](https://posit.co/products/open-source/rstudio-server/), also hosted on Fly, which allows for hassle-free editing of the [RDB
    report](https://report.rdb.vote/).

The [DNS](https://en.wikipedia.org/wiki/Domain_Name_System) entries for the `rdb.vote` domain involved in the above setup are recorded in [`dns_records.toml` in
the `zdaarau/websites/rdb.vote` repository](https://gitlab.com/zdaarau/websites/rdb.vote/-/blob/main/dns_records.toml).

[^1]: There is one notable open-source alternative to PostgREST: [pRESTd](https://github.com/prest/prest), written in Go. Once OpenAPI spec auto-generation [is
    implemented](https://github.com/prest/prest/issues/426), we could have a closer look at it.

## DBMS

We use ["serverless"](https://en.wikipedia.org/wiki/Serverless_computing)[^2] [PostgreSQL](https://en.wikipedia.org/wiki/PostgreSQL) by
[Neon](https://neon.tech/docs/introduction/about)[^3] as our Database Management System (DBMS). We use their [*launch*
plan](https://neon.tech/docs/introduction/plans#launch) which meets our current needs[^4].

Neon enhances PostgreSQL with additional features like [branching](https://neon.tech/docs/introduction/branching) and
[autoscaling](https://neon.tech/docs/introduction/autoscaling) and supports [a number of popular PostgreSQL
extensions](https://neon.tech/docs/extensions/pg-extensions).

[^2]: See [Neon architecture](https://neon.tech/docs/introduction/architecture-overview) and [Postgres
    compatibility](https://neon.tech/docs/reference/compatibility) for technical details.

[^3]: Neon offers a fully managed PostgreSQL server platform. The risk of [vendor lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in) is rather low since

    1.  it [can be self-hosted](https://community.neon.tech/t/can-neon-be-self-hosted/51) and the core parts of the Neon software architecture including the
        Rust-written [storage engine](https://github.com/neondatabase/neon) are [open-sourced](https://github.com/neondatabase/) under
        [permissive](https://en.wikipedia.org/wiki/Permissive_software_license) licenses that should enable competitors to create compatible services without
        too much engineering costs. Only Neon's user interface front-end parts like the [RESTful API](https://neon.tech/docs/reference/glossary#neon-api) and
        the [Neon Console](https://neon.tech/docs/reference/glossary#neon-console) are [kept
        closed-source](https://community.neon.tech/t/help-needed-for-self-hosting/1503/2) and would need to be re-implemented by competing offerings.
    2.  the distinct Neon features like branching, although very convenient, aren't an absolute requirement for our use case and we could switch to [another
        managed PostgreSQL provider](https://fly.io/docs/postgres/getting-started/what-you-should-know/#recommended-external-providers) like
        [Supabase](https://supabase.com/database) or [Tembo](https://tembo.io/), use Fly's integrated [Supabase
        Postgres](https://fly.io/docs/reference/supabase/) or run our own PostgreSQL cluster, e.g. [on
        Fly](https://fly.io/docs/postgres/getting-started/create-pg-cluster/) or [on Kubernetes](https://cloudnative-pg.io/).

[^4]: The launch plan is limited to a compute size of up to 4 vCPUs and 16 GB RAM and includes 10 GiB of storage and 300 compute hours. Additional resources are
    pay-as-we-go. We can always upgrade our plan to get access to higher CPU and RAM limits.

### Data model

In a data model diagram, the RDB looks as follows:

[![RDB data model
diagram](https://gitlab.com/zdaarau/rpkgs/rdb/-/raw/pg/man/figures/rdb_dm.png?ref_type=heads)](https://pg--rdb-rpkg-dev.netlify.app/reference/figures/rdb_dm.svg)

### Neon.tech setup details

-   Neon project
    -   Name: `neon_rdb`
    -   ID: `curly-rain-52896163`
    -   AWS region: `aws-eu-central-1` (Europe, Frankfurt)
    -   PostgreSQL major version[^5]: 16
    -   [Default branch](https://neon.tech/docs/manage/branches#default-branch)
        -   Name: `main`
        -   ID: `br-wandering-darkness-a2sg63zq`
        -   Compute instances:
            -   *REDACTED* (primary; auto-scales from 0.25–4 [CU](https://neon.tech/docs/reference/glossary#compute-unit-cu)s)
            -   `ep-frosty-flower-a28keh10` (read replica; auto-scales from 0.25–2 CUs)
    -   Testing branch[^6]
        -   Name: `testing`
        -   Parent: `main`
        -   ID: `br-steep-morning-a21x0mxx`
        -   Compute instances:
            -   *REDACTED* (primary; auto-scales from 0.25–4 CUs)
    -   PostgreSQL database
        -   Name: `rdb`
        -   ID: `100730688`

[^5]: Each Neon project is created with a specific PostgreSQL major version. A new major version is released approximately once per year. New major versions
    usually come with powerful new features and performance improvements but introduce backwards-incompatible changes and thus require recreating the database
    after an upgrade, e.g. via a [`pg_dump`](https://www.postgresql.org/docs/current/app-pgdump.html) on the old PostgreSQL server version followed by a
    [`pg_restore`](https://www.postgresql.org/docs/current/app-pgrestore.html) on the new server version. See [*Neon Postgres Version Support
    Policy*](https://neon.tech/docs/postgresql/postgres-version-policy) for further details.

    Since we already have the necessary tooling to back up and recreate the RDB from scratch in the form of `rdb::reset_rdb()` & Co. (see
    [below](#initialization)), we can simply rely on that. Before upgrading to a new PostgreSQL major version, it's recommended to wait for the first minor
    version release to minimize the chance of running into regressions. Furthermore we must wait for all the PostgreSQL extensions we rely on (currently
    `insert_username`, `moddatetime` and `pg_graphql`) to be ported to the new major version and for Neon.tech to approve them. The list of supported PostgreSQL
    extensions on Neon.tech by major version is available [here](https://neon.tech/docs/extensions/pg-extensions).

[^6]: We maintain a permanent `testing` branch that is based on the `main` branch and allows us to test client software updates, database migrations etc.

### Initialization

The RDB DBMS is tightly coupled to the [**rdb**](https://pg--rdb-rpkg-dev.netlify.app/) R package[^7], which i.a. contains various [administration
functions](https://pg--rdb-rpkg-dev.netlify.app/reference/#administration), including
[`rdb::reset_rdb()`](https://pg--rdb-rpkg-dev.netlify.app/reference/reset_rdb) that allows to (re-)create the complete NocoDB server and PostgreSQL database
while retaining all relevant data of the latter.

Consequently, the raw SQL code that initializes the whole PostgreSQL schema is [part of the R
package](https://gitlab.com/zdaarau/rpkgs/rdb/-/tree/pg/inst/sql?ref_type=heads). Several lower-level functions like
[`rdb:::pg_init_db()`](https://pg--rdb-rpkg-dev.netlify.app/reference/pg_init_db),
[`rdb:::pg_reset_db()`](https://pg--rdb-rpkg-dev.netlify.app/reference/pg_reset_db),
[`rdb::pg_init_aux_tbls()`](https://pg--rdb-rpkg-dev.netlify.app/reference/pg_init_aux_tbls) and
[`rdb::pg_init_main_tbls()`](https://pg--rdb-rpkg-dev.netlify.app/dev/reference/pg_init_main_tbls) form the underlying building blocks for `rdb::reset_rdb()`.

After `rdb::reset_rdb()`, a few additional steps are necessary to configure NocoDB to our needs. Taken together, the steps to rebuild the *whole* database setup
include:

<details>
<summary>
Steps
</summary>

1.  Reset NocoDB to factory settings via

    ``` r
    rdb:::purge_nocodb()
    rdb::reset_rdb(reset_db = TRUE)
    ```

2.  Apply the remaining NocoDB configuration changes we can't automate (yet):

    1.  Enable `Show NULL in Cells` in base settings.

    2.  Change the UI data type of the following columns:

        -   `legal.norms.url` to `URL`
        -   `referendums.attachments` to `Attachment`

    3.  Activate the [<kbd>Enable Rich Text<kbd> toggle](https://docs.nocodb.com/fields/field-types/text-based/rich-text/#create-a-rich-text-field) for all
        relevant table columns. They can be listed via:

        ``` r
        col_names_rich_text <-
          rdb:::ncdb_col_metadata |>
          dplyr::filter(meta.richMode) |>
          dplyr::pull(col_name)

        rdb:::tbl_metadata$name |>
          purrr::map(\(tbl_name) {

            rdb:::ncdb_tbl_cols(tbl_id = rdb:::ncdb_tbl_id(tbl_name = tbl_name)) |>
              dplyr::filter(column_name %in% !!col_names_rich_text) |>
              dplyr::pull(column_name) |>
              tibble::tibble(tbl_name = tbl_name,
                             col_name = _)
          }) |>
          purrr::list_rbind()
        ```

    4.  Change column titles of NocoDB's "virtual" `LinkToAnotherRecord` columns according to `rdb:::ncdb_col_renames`.

    5.  Remove the `referendum_` prefix from column titles of NocoDB's "virtual" `Links` columns in the `referendums` table.

    6.  Reorder and show/hide all columns as desired.

</details>

[^7]: Currently, development for the *relational* RDB (which runs on PostgreSQL) takes place in the [`pg`
    branch](https://gitlab.com/zdaarau/rpkgs/rdb/-/tree/pg?ref_type=heads). Once the whole setup is stable enough, i.e. ready for production use, it will be
    merged into the default `master` branch.

### Backup and restore

A history of changes for all branches is retained for 7 days[^8], which enables [point-in-time
restoration](https://neon.tech/docs/introduction/point-in-time-restore) within this timeframe.

Beyond this 7-days window, we store [weekly full snapshots of the complete PostgreSQL database to two different object storage services](fly_backup).

[^8]: This could be extended to 30 days by upgrading to [Neon's *Scale* plan](https://neon.tech/docs/introduction/plans#scale).

### Frequently used commands

#### Start/suspend compute instance

Neon's [RESTful API](https://api-docs.neon.tech/reference/getting-started-with-neon-api) can be used to i.a. (re)start or suspend a compute instance. To do so
via cURL, run:

``` sh
# suspend our read replica instance
curl --request POST \
     --url 'https://console.neon.tech/api/v2/projects/curly-rain-52896163/endpoints/ep-frosty-flower-a28keh10/suspend' \
     --header 'accept: application/json' \
     --header "authorization: Bearer ${NEON_API_KEY}"

# start our read replica instance
curl --request POST \
     --url 'https://console.neon.tech/api/v2/projects/curly-rain-52896163/endpoints/ep-frosty-flower-a28keh10/start' \
     --header 'accept: application/json' \
     --header "authorization: Bearer ${NEON_API_KEY}"
```

#### Branching

We can create additional project branches, e.g. for testing purposes, anytime via the [Neon CLI](https://neon.tech/docs/reference/neon-cli). To create a new
branch named `playground` that is based on the `main` branch and has a read-write compute instance assigned, run:

``` sh
neonctl branches create --project-id curly-rain-52896163 \
                        --name playground \
                        --parent main \
                        --compute \
                        --type read_write
```

To delete that branch again, run:

``` sh
neonctl branches delete playground --project-id curly-rain-52896163
```

## Credentials

An encrypted backup of all credentials for the RDB setup is stored in [`credentials.md.age`](credentials.md.age).

The file was created using [*age*](https://github.com/FiloSottile/age#readme)[^9]:

``` sh
age --encrypt --passphrase --output credentials.md.age credentials.md
```

It can be decrypted via (will prompt for the passphrase):

``` sh
age --decrypt --output credentials.md credentials.md.age
```

[^9]: *age* is a simple, modern and secure file encryption tool, format, and Go library. It is available for all major platforms, see the [installation
    instructions](https://github.com/FiloSottile/age#installation).

## License

Code and configuration in this repository is licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[`LICENSE.md`](LICENSE.md).

----------------------------------------------------------------------------------------------------------------------------------------------------------------
