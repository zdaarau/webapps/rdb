# RDB RStudio Server

[![Health](https://status.rdb.vote/api/v1/endpoints/internal-tools_rstudio-server/health/badge.svg)](https://status.rdb.vote/endpoints/internal-tools_rstudio-server)
[![Uptime (30 days)](https://status.rdb.vote/api/v1/endpoints/internal-tools_rstudio-server/uptimes/30d/badge.svg)](https://status.rdb.vote/endpoints/internal-tools_rstudio-server)

We run a preconfigured [RStudio Server](https://posit.co/products/open-source/rstudio-server/) instance on [Fly](https://fly.io/) that allows for hassle-free
editing of several Quarto projects including the [RDB report](https://report.rdb.vote/). It is accessible under
[**`rstudio.rdb.vote`**](https://rstudio.rdb.vote/).

The image is based on our custom [RStudio Server container](https://gitlab.com/zdaarau/webapps/rstudio/). See the latter for underlying details. This repository
contains non-sensitive RStudio Server configuration. Sensitive configuration data is stored via [Fly secrets](https://fly.io/docs/reference/secrets/) and in a
local `.secrets` file excluded from this repository.

Sensitive environment variables to be set as Fly secrets include:

-   `GH_TOKEN`: GitHub access token to avoid rate-limiting R package installations from GitHub. A token with access scopes `repo` and `read:org` is sufficient.

-   `GIT_REPOS`: HTTPS URLs of Git repositories to be set up for all RStudio users on persistent storage. Private repositories must include credentials[^1] in
    the form `https://USERNAME:TOKEN@gitlab.com/GROUP/SUBGROUPS/PROJECT.git`.

    Currently, `GIT_REPOS` is supposed to include HTTPS URLs for the following GitLab repositories:

    -   [RDB Report](https://gitlab.com/zdaarau/rpkgs/rdb.report)
    -   [IPSA Paper](https://gitlab.com/zdaarau/private/ipsa_paper/)
    -   [JDD Paper](https://gitlab.com/zdaarau/private/jdd_paper/)

Sensitive environment variables to be set in a local `.secrets` text file include `BATCH_USER_CREATION` and `PASSWORD`, see
[here](https://gitlab.com/zdaarau/webapps/rstudio#user-configuration) for details.

Users' Git and SSH config files, expected to live in `user_home_dir/{username}/.gitconfig` and `user_home_dir/{username}/.ssh/`, are not included in this
repository for security/privacy reasons.

[^1]: Regular GitLab access tokens [expire after 12 months at the latest](https://about.gitlab.com/blog/2023/10/25/access-token-lifetime-limits/), thus we use a
    [***service account*** token](https://docs.gitlab.com/ee/api/group_service_accounts.html#create-a-personal-access-token-for-a-service-account-user) with
    [**no expiration
    date**](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-service-account-personal-access-token-with-no-expiry-date) and
    [`read_api` access scope](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-token-scopes)) to avoid token rotation
    maintenance work.

## Hosting details

The Fly [app](https://fly.io/docs/reference/apps/) is named `rdb-rstudio`.

Currently, it runs on a [`performance-2x` VM with *8 GB* RAM](https://fly.io/docs/about/pricing/#compute) and a 20 GiB[^2] [persistent storage
volume](https://fly.io/docs/reference/volumes/) attached named `rdb_rstudio`[^3], hosted in the [*Paris, France*
region](https://fly.io/docs/reference/regions/). Should it prove necessary to expand performance, we could [increase
RAM](https://fly.io/docs/flyctl/scale-memory/) and/or [switch to a faster CPU](https://fly.io/docs/flyctl/scale-vm/) anytime[^4].

The Fly app is configured to automatically scale to zero within 2 minutes[^5] without active connections. This means that **the whole VM is stopped at most
2 minutes after the last user terminates their RStudio session or the last active RStudio session timed out**[^6]. The VM is automatically started again on the
next connection attempt. Thus, users might experience a small delay to load the login page[^7] if the VM hasn't already been running before.

Users ***should*** make sure to **commit and push** their pending changes before quitting their session[^8]. Although changes on persistent storage (mounted at
`/persistent`) are retained between server restarts, it makes updating the Git repositories after deployment hassle-free if there are no pending changes – and
[re-deployments](#deployment) are supposed to happen regularly.

[^2]: The volume size can always be [extended](https://fly.io/docs/flyctl/volumes-extend/). To extend it to 40 GiB for example, simply run:

    ``` sh
    flyctl volumes extend vol_24yxnmey09d51x9v --size=40
    ```

    Note that this will restart the app.

[^3]: Its unique ID is `vol_24yxnmey09d51x9v`.

[^4]: RAM is typically more critical for R since [it's mostly single-threaded but often copies on
    reference](https://community.rstudio.com/t/rstudio-server-system-sizing-capacity-planning/14264/2).

[^5]: The delay is set in the crontab file `/etc/crontab`, see the base image
    [`Dockerfile`](https://gitlab.com/zdaarau/webapps/rstudio/-/blob/main/Dockerfile?ref_type=heads).

[^6]: An active RStudio session times out, i.e. is killed, after 1 hour of inactivity (set via [`session-timeout-minutes=60` in the base
    image](https://gitlab.com/zdaarau/webapps/rstudio/-/blob/main/global_rstudio_config/rsession.conf?ref_type=heads)).

[^7]: If the login page isn't loaded within \~5 seconds, refresh the browser window. It regularly happens that the web browser's initial connection to Fly's
    proxy gets stale, i.e. is not properly redirected to the started RStudio Server instance.

[^8]: A session is ended by pressing the red power button on the top right and then closing the web browser tab. This is strongly recommended to save on
    resources, i.e. energy.

## Admin access

We can directly connect into the root filesystem of the `rdb-rstudio` app via an [SSH tunnel](https://fly.io/docs/flyctl/ssh-console/) to perform any run-time
administration tasks. To log in over SSH, run:

``` sh
flyctl ssh console --app rdb-rstudio
```

## Deployment

To update the software stack, [update our base image](https://gitlab.com/zdaarau/webapps/rstudio#update) and adapt the version in the `.Dockerfile` accordingly.

Deployment is largely automated. To deploy a new release, simply run:

``` sh
./deploy.sh
```

If there are RStudio user sessions active, the script will tell you so and ask whether you still want to proceed.

After deploying the app, the script automatically attempts to update the persistent storage on the newly deployed machine (which must be done once after each
deployment to guarantee a clean state). Should that fail for some reason, the script will tell so and we need to manually run:

``` sh
flyctl ssh console --command '/update_persistent.sh' --app rdb-rstudio
```

Git repositories with uncommitted changes pending are skipped by `update_persistent.sh` and a corresponding `WARNING` message is printed.

## Add new user

Follow the instructions [here](https://gitlab.com/zdaarau/webapps/rstudio/#add-a-new-rstudio-user) to add a new user to the RStudio Server.

----------------------------------------------------------------------------------------------------------------------------------------------------------------
