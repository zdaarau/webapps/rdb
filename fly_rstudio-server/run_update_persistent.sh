#!/bin/bash
#
# Update persistent storage on RStudio Server Fly app
#
# Requirements:
#
# - CLI tools:
#   - [Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) (obviously)
#   - [coreutils](https://en.wikipedia.org/wiki/List_of_GNU_Core_Utilities_commands)
#   - [`dasel`](https://daseldocs.tomwright.me/)
#   - [`flyctl`](https://fly.io/docs/flyctl/)
#   - [`sd`](https://github.com/chmln/sd)

# define convenience functions
get_fly_status() {
  FLY_CONFIG="$(flyctl status --json)"
  FLY_MACHINE_ID="$(echo "${FLY_CONFIG}" | dasel --read json --selector 'Machines.[0].id' | sd --fixed-strings '"' '')"
  FLY_MACHINE_STATE="$(echo "${FLY_CONFIG}" | dasel --read json --selector 'Machines.[0].state' | sd --fixed-strings '"' '')"
  FLY_MACHINE_CHECK_STATUS="$(echo "${FLY_CONFIG}" | dasel --read json --selector 'Machines.[0].checks.[0].status' | sd --fixed-strings '"' '')"
}

# start the Fly machine after it has been replaced
get_fly_status
while [ "${FLY_MACHINE_STATE}" == 'replacing' ] ; do
  sleep 1
  get_fly_status
done
flyctl machine start "${FLY_MACHINE_ID}"
get_fly_status
while [ "${FLY_MACHINE_STATE}" == 'started' ] && [ "${FLY_MACHINE_CHECK_STATUS}" == 'warning' ] ; do
  sleep 1
  get_fly_status
done

sleep 1

# initialize persistent storage
if [ "${FLY_MACHINE_STATE}" == 'started' ] && [ "${FLY_MACHINE_CHECK_STATUS}" == 'passing' ] ; then
  flyctl ssh console --command '/update_persistent.sh'
else
  echo "Couldn't initialize persistent storage because Fly machine is in state '${FLY_MACHINE_STATE}' and its health check status is '${FLY_MACHINE_CHECK_STATUS}'."
  echo "Please manually run \`flyctl ssh console --command '/update_persistent.sh' --app ${FLY_APP_NAME}\` to rectify."
fi
