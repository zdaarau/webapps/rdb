#!/bin/bash

# iterate over all users
mapfile -t users < <(ls /home/)

for user in "${users[@]}"; do

  uid=$(id --user ${user})

  # create RStudio user files to preset project paths
  ## default project
  mkdir -p "/home/${user}/.local/share/rstudio/projects_settings"
  echo -e "/persistent/${user}/git_repos/rdb.report/input/report/report.Rproj" > "/home/${user}/.local/share/rstudio/projects_settings/last-project-path"
  chown --recursive "${uid}":"${uid}" "/home/${user}/.local/share/rstudio/projects_settings/last-project-path"

  ## recently used projects
  # shellcheck disable=SC2088
  mkdir -p "/home/${user}/.local/share/rstudio/monitored/lists"
  echo -e "/persistent/${user}/git_repos/jdd_paper/jdd_paper.Rproj\n/persistent/${user}/git_repos/ipsa_paper/ipsa_paper.Rproj\n/persistent/${user}/git_repos/rdb.report/input/report/report.Rproj" > "/home/${user}/.local/share/rstudio/monitored/lists/project_mru"
  chown --recursive "${uid}":"${uid}" "/home/${user}/.local/share/rstudio/monitored/lists/project_mru"

done
