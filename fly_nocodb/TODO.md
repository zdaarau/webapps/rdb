# TODOs

-   configure CSPs in `fly.toml`

-   increase [`DB_QUERY_LIMIT_*`](https://docs.nocodb.com/getting-started/self-hosted/environment-variables/)?

### open issues

#### yet to submit

##### auto-filled columns

-   Bug: Creating a new column of type `CreatedBy` (or `LastModifiedBy`) in a table from an external PostgreSQL data source doesn't work properly

    -   Both creating via UI and API show the same issue

    -   NocoDB creates a new column `created_by` in the PG table (as expected), but adds *two* columns to its internal metadata:

        INSERT COLS HERE

        At this point, the `created_by` column works as expected, i.e. is auto-filled by NocoDB and visible/usable from its UI. But the next time the DB schema
        is synced ("meta sync"), NocoDB will detect a deleted column in the table (sync state `Column removed(null)`), as if it assumed that two columns would
        have been created before). After performing the meta sync, the non-system `created_by` column disappears from NocoDB's internal metadata **and the
        `created_by` field is no more visible/accessible via the UI**. The `created_by` column is still there in the external DB and NocoDB continues to fill it
        with the correct user IDs, but we can't use it anymore via the NocoDB UI. WTF?

-   Feature: Use `timestamp with time zone DEFAULT CURRENT_TIMESTAMP` instead of `timestamp without time zone` for `CreatedTime` and `LastModifiedTime` fields
    in external PostgreSQL DB

    -   currently, NocoDB acts as the "centre of the universe" reagarding this, i.e. resulting in users of the external DB having to figure out the timezone of
        the NocoDB server and correct the dates accordingly before writing them to the DB if their timezone isn't equal to the server's timezone.

-   Feature: Allow to change existing field into any of the auto-filled `CreatedBy/Time` and `LastModifiedBy/Time` `uidt` types

##### docs

-   required request body parameter `childViewId` is missing from
    [`PATCH /api/v2/meta/columns/{id_col}`](https://meta-apis-v2.nocodb.com/#tag/DB-Table-Column/operation/db-table-column-update) API endpoint documentation

##### other

-   Question: how to lock a table's default grid view? the docs [suggest this is already
    possible](https://docs.nocodb.com/views/views-overview/#view-permission-types), but the screenshot doesn't match what I see on latest NocoDB...

    -   test locking view via API!

-   Bug: Improve the [Update Column](https://meta-apis-v2.nocodb.com/#tag/DB-Table-Column/operation/db-table-column-create) API endpoint, cf.
    https://github.com/nocodb/nocodb/issues/7832#issuecomment-2002197438 and related to https://github.com/nocodb/nocodb/issues/833

    Maybe just request a more consequent separation of concerns between external DB and NocoDB augmentation (virtual columns etc.), which should solve this
    issue as well as many other related pain points.

-   Bug: Rich-text / Markdown links don't work in comments (neither via UI nor Markdown syntax).

-   Feature: Improve error display

    -   errors from the underlying DB (e.g. caused by table constraints) can be very useful/important to NocoDB users. Currently, errors are displayed in a
        single line like this

        `````
        Add row failed: insert into "referendum_sub_votes" ("administrative_unit_id", "count", "created_by", "option_display", "referendum_id", "updated_by") values ('AD', 99, 'us230d11dvng1zu5', 'no', 1, NULL) returning "referendum_id" as "c5bqkkv25xumus6" - The referendum's administrative unit must be the parent of the sub vote's administrative unit.
        ```

        Instead, I'd suggest to display the above as follows, with rich formatting (example is in Markdown syntax):

        ````md
        Add row failed: The referendum's administrative unit must be the parent of the sub vote's administrative unit.

        <display><summary>Failed SQL statement:</summary>

        ```sql
        insert into "referendum_sub_votes" ("administrative_unit_id", "count", "created_by", "option_display", "referendum_id", "updated_by") values ('AD', 99, 'us230d11dvng1zu5', 'no', 1, NULL) returning "referendum_id" as "c5bqkkv25xumus6"
        ```

        </display>
        `````

        This would be much more readable and useful for users.

        Also related to https://github.com/nocodb/nocodb/issues/8809.

-   Feature: [Form views](https://docs.nocodb.com/views/view-types/form/) for *internal* use, i.e. with auth by individual users (via `xc-auth`/`xc-token`
    header)

    -   already on the roadmap, [it appears](https://github.com/nocodb/nocodb/issues/7077#issuecomment-2028613502)
    -   also related: <https://github.com/nocodb/nocodb/issues/7235>

-   Feature: Use [`jsonb`](https://www.postgresql.org/docs/current/datatype-json.html) data type for attachment columns in external PostgreSQL data sources

-   Feature: More standard Markdown:

    -   do not escape backticks (for code fences and inline code) when inputting text via the UI, cf. https://github.com/nocodb/nocodb/issues/9172
    -   store newlines as `\n` instead of HTML (`<br />`)
    -   store code fences as native Markdown instead of only NocoDB-internal

-   Feature: better UI for rich text editing: currently, it's not possible to exit block elements like code blocks or citations again (instead, you have to
    retroactively mark the desired paragraphs formatted as such and you can't mark the last paragraph without losing the ability to exit again)

-   Feature: Allow to search arbitrary fields when linking records (currently, only [display value](https://docs.nocodb.com/fields/display-value/) field can be
    searched)

-   Feature: generated/composite display values

-   Feature: Introduce option to hide self-promo ("Try NocoDB cloud" at the bottom of main UI sidepanel; "WE ARE HIRING!!!" popover on OpenAPI desc pages) and
    social media links in user menu (Discord, Reddit, Twitter) when self-hosting

#### essential

(in decreasing order of importance)

-   [Bug: Table with `GENERATED ALWAYS AS (generation_expr) STORED PRIMARY KEY` or `NOT NULL GENERATED ALWAYS AS (generation_expr) STORED` column not fillable
    from NocoDB](https://github.com/nocodb/nocodb/issues/9278)
    -   it was tried to work around this issue for the time being by creating a suitable trigger function, but as it turned out, [this is basically impossible
        due to technical limitations in PostgreSQL](https://stackoverflow.com/questions/52181919/postgresql-set-default-value-in-trigger)

        ideal trigger function and trigger (if `NEW.display := DEFAULT;` would work):

        ``` sql
        -- Create function and trigger to work around [this NocoDB issue](https://github.com/nocodb/nocodb/issues/9278)
        CREATE OR REPLACE FUNCTION remove_options_display_for_nocodb()
        RETURNS TRIGGER
          LANGUAGE plpgsql
          AS $$
              BEGIN
              NEW.display := DEFAULT;

              RETURN NEW;
              END;
          $$;

        CREATE OR REPLACE TRIGGER remove_options_display_for_nocodb
          BEFORE INSERT ON public.options
          FOR EACH ROW
          WHEN (CURRENT_USER = 'nocodb')
          EXECUTE PROCEDURE public.remove_options_display_for_nocodb();
        ```
-   [Bug: Impossible to add rows via form view to table that has only foreign keys](https://github.com/nocodb/nocodb/issues/9365)
-   [Bug: "T is not iterable" when trying to delete records](https://github.com/nocodb/nocodb/issues/9307)
-   [Bug: Impossible to alter multiple `LinkToAnotherRecord` values in a single transaction](https://github.com/nocodb/nocodb/issues/9281)
-   [Bug: Impossible to directly fill "Has many" field when creating record via form view](https://github.com/nocodb/nocodb/issues/8768)
-   [Bug: PostgreSQL column of type `date` is misinterpreted for dates \< `0100-01-01`](https://github.com/nocodb/nocodb/issues/8753)
-   [Doc: `/api/v2/meta/integrations` missing from RESTful API documentation](https://github.com/nocodb/nocodb/issues/9406)
-   [Bug: Hide RO columns in new form](https://github.com/nocodb/nocodb/issues/9312)
-   [Feature: enum for Single Select in Postgres](https://github.com/nocodb/nocodb/issues/4862)
-   [Feature: Support for PostgreSQL arrays](https://github.com/nocodb/nocodb/issues/593)
-   [Feature: Auto-update `created_by` and `updated_by` columns on insertion/update for externally created tables](https://github.com/nocodb/nocodb/issues/7796)
-   [Feature: Show actual database error message on failed SQL operation](https://github.com/nocodb/nocodb/issues/8809)
-   [Feature: Set Configuration Locked but not the Tables' Cells](https://github.com/nocodb/nocodb/issues/5374)
-   [Feature: Handle `USING` row-level security policy violations in external (PostgreSQL) DB gracefully](https://github.com/nocodb/nocodb/issues/9008)
-   [Feature: Allow choosing junction table and columns for m2m relationship](https://github.com/nocodb/nocodb/issues/8241)
-   [Feature: New User Role - Form-Submitter / Form submitted by](https://github.com/nocodb/nocodb/issues/7077)
-   [Feature: Resolve comments](https://github.com/nocodb/nocodb/issues/4256) (feature currently [limited to NocoDB
    Cloud](https://github.com/nocodb/nocodb/issues/4256#issuecomment-2167892493))
-   [Feature: Mention team members in comments](https://github.com/nocodb/nocodb/issues/4257) (feature currently [limited to NocoDB
    Cloud](https://github.com/nocodb/nocodb/issues/4257#issuecomment-2167891832))
-   [Feature: Alias for column names](https://github.com/nocodb/nocodb/issues/716)
-   [Feature: Sort enum based on order](https://github.com/nocodb/nocodb/issues/7538)
-   [Feature: Custom roles](https://github.com/nocodb/nocodb/issues/2712)
-   [Feature: Support changing `Show M2M Tables` and `Show NULL in Cells` base settings via RESTful API](https://github.com/nocodb/nocodb/issues/8663)

#### nice-to-have

(in decreasing order of importance)

-   [Bug: Attachments are not actually deleted from S3 bucket when removed from cell](https://github.com/nocodb/nocodb/issues/7895)
-   [Bug: Update base with `meta` data via API doesn't work](https://github.com/nocodb/nocodb/issues/8417)
-   [Bug: Initial invitation e-mail is sent twice](https://github.com/nocodb/nocodb/issues/8460)
-   [Bug: Signup page for self-hosted NocoDB should exclude newsletter signup toggle and ToS footer text](https://github.com/nocodb/nocodb/issues/8459)
-   [Bug: Incorrect CURRENT_TIMESTAMP default on SQLite tables](https://github.com/nocodb/nocodb/issues/6984)
-   [Bug: Comments added through API TOKEN request cannot be displayed, but are displayed normally through JWT
    TOKEN](https://github.com/nocodb/nocodb/issues/7132)
-   [Bug: NocoDB can't sync data if multiple many-to-many relations with constraints named the same exist](https://github.com/nocodb/nocodb/issues/4442)
-   [Bug: REST API where parameter with null check on date fields](https://github.com/nocodb/nocodb/issues/9157)
-   [Bug: text to numerical type conversion ignores negatives](https://github.com/nocodb/nocodb/issues/9417)
-   [Feature: Option to select row color](https://github.com/nocodb/nocodb/issues/8414) (dynamically colorize rows based on column value)
-   [Feature: Option to pull "friendly" form field names from column comments field](https://github.com/nocodb/nocodb/issues/3571)
    -   TODO: update [this comment](https://github.com/nocodb/nocodb/issues/3571#issuecomment-1981871700) accordingly
-   [Feature: Security Enhancement](https://github.com/nocodb/nocodb/issues/5225)
-   [Feature: Two factor authentication (2FA)](https://github.com/nocodb/nocodb/issues/2731)
-   [Feature: Allow to set global default for interpreting `LongText` columns as Markdown ("rich text")](https://github.com/nocodb/nocodb/issues/7832)
-   [Feature: Default search field on a per-view basis](https://github.com/nocodb/nocodb/issues/9271)
-   [Feature: read-only mode or swagger.json import](https://github.com/nocodb/nocodb/issues/318)
-   [Feature: Lock selective records from editing](https://github.com/nocodb/nocodb/issues/9337)
-   [Feature: Support copy/paste of multiple cells](https://github.com/nocodb/nocodb/issues/8342)
-   [Feature: Infinite Scroll on table](https://github.com/nocodb/nocodb/issues/3568)
-   [Feature: Make whole `LinkToAnotherRecord` field clickable when empty](https://github.com/nocodb/nocodb/issues/7819)
-   [Feature: Support enabling invite-only signup via `NC_INVITE_ONLY_SIGNUP` env var again](https://github.com/nocodb/nocodb/issues/7814)
-   [Feature: Support for PG materialized views](https://github.com/nocodb/nocodb/issues/1157)
-   [Feature: Introduce `Bulk Delete` button for attachment fields](https://github.com/nocodb/nocodb/issues/9282)
-   [Feature: Display M2M Tables name near every m2m relationship](https://github.com/nocodb/nocodb/issues/8553)
-   [Feature: Edit email template(s)](https://github.com/nocodb/nocodb/issues/7085)
-   [Feature: ACL for api token](https://github.com/nocodb/nocodb/issues/1902)
-   [Feature: Timeline View](https://github.com/nocodb/nocodb/issues/7078)
-   [Feature: Allow field header height to be adjustable](https://github.com/nocodb/nocodb/issues/8959)
-   [Feature: Ability to single click copy image URL](https://github.com/nocodb/nocodb/issues/8923)
-   [Feature: Matrix.org notifications](https://github.com/nocodb/nocodb/issues/741)
-   [Feature: Allow for mail templates translation](https://github.com/nocodb/nocodb/issues/8902)
