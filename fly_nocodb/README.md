# RDB NocoDB

[![Health](https://status.rdb.vote/api/v1/endpoints/internal-tools_admin-(nocodb)/health/badge.svg)](https://status.rdb.vote/endpoints/internal-tools_admin-(nocodb))
[![Uptime (30 days)](https://status.rdb.vote/api/v1/endpoints/internal-tools_admin-(nocodb)/uptimes/30d/badge.svg)](https://status.rdb.vote/endpoints/internal-tools_admin-(nocodb))
[![Response time (30 days)](https://status.rdb.vote/api/v1/endpoints/internal-tools_admin-(nocodb)/response-times/30d/badge.svg)](https://status.rdb.vote/endpoints/internal-tools_admin-(nocodb))

The front end for data coders to edit RDB entries is provided by a [NocoDB](https://www.nocodb.com/) server hosted on [Fly](https://fly.io/). It is accessible
under [**`admin.rdb.vote`**](https://admin.rdb.vote/).

This repository contains the non-sensitive NocoDB configuration. Sensitive configuration data is stored via [Fly
secrets](https://fly.io/docs/reference/secrets/). Currently this includes the following [NocoDB environment
variables](https://docs.nocodb.com/getting-started/self-hosted/environment-variables):

-   `LITESTREAM_AGE_SECRET_KEY`
-   `LITESTREAM_S3_ACCESS_KEY_ID`
-   `LITESTREAM_S3_SECRET_ACCESS_KEY`
-   `NC_ADMIN_EMAIL`
-   `NC_ADMIN_PASSWORD`
-   `NC_AUTH_JWT_SECRET`
-   `NC_SMTP_PASSWORD`
-   `NC_SMTP_USERNAME`

## Hosting details

The Fly [app](https://fly.io/docs/reference/apps/) is named `rdb-nocodb` and part of the `RDB` organisation.

We use an [SQLite](https://en.wikipedia.org/wiki/SQLite) database for NocoDB's metadata and run a single [`shared-cpu-1x` instance with *512 MB*
RAM](https://fly.io/docs/about/pricing/#compute) hosted in the [*Paris, France* region](https://fly.io/docs/reference/regions/). Should it prove necessary to
expand performance for our main users (located in Switzerland), we could [increase RAM](https://fly.io/docs/flyctl/scale-memory/) and/or [switch to a faster
CPU](https://fly.io/docs/flyctl/scale-vm/) anytime[^1].

Should we also want to provide fast access for non-European users, we could [run multiple instances](https://fly.io/docs/apps/scale-count/) of NocoDB [in
multiple regions](https://fly.io/docs/apps/scale-count/#scale-an-app-s-regions). This would either require to replicate the SQLite database using
[LiteFS](https://fly.io/docs/litefs/), or to switch using our Neon PostgreSQL database to store NocoDB's metadata. However, this most probably won't be
necessary in the forseeable future.

The NocoDB metadata is continuously backed up to the private [Backblaze B2](https://www.backblaze.com/cloud-storage)[^2] object storage bucket `rdb-nocodb`[^3]
as well as restored from there when the Fly app is (re-)deployed thanks to [Litestream](https://litestream.io/). Hence there's no need to attach a [persistent
storage volume](https://fly.io/docs/reference/volumes/) to the Fly app. The backed up data is fully encrypted using [Litestream's age
integration](https://litestream.io/reference/config/#encryption), whose strong public-key cryptography ensures neither Backblaze nor any eavesdropper can read
the data. Consequently, there is no point in enabling [Backblaze's server-side
encryption](https://www.backblaze.com/docs/cloud-storage-server-side-encryption#serverside-encryption-options) (which would still involve blindly trusting
Backblaze).

NocoDB is configured to store [file attachments](https://docs.nocodb.com/fields/field-types/custom-types/attachment) to the public Backblaze B2 bucket
`rdb-attachments`[^4].

[^1]: Note that scaling automatically restarts the app. The most relevant documentation on (auto)scaling Fly apps includes:

    -   [Scale Machine CPU and RAM](https://fly.io/docs/apps/scale-machine/)
    -   [Scale the Number of Machines](https://fly.io/docs/apps/scale-count/)
    -   [Automatically Stop and Start Machines](https://fly.io/docs/apps/autostart-stop/)

[^2]: Backblaze B2 is an [Amazon-S3-compatible](https://en.wikipedia.org/wiki/Amazon_S3#S3_API_and_competing_services) object storage service that is
    [considerably cheaper](https://www.backblaze.com/cloud-storage/pricing).

[^3]: The bucket is hosted in the [EU-Central region](https://help.backblaze.com/hc/en-us/articles/217664578-Where-are-my-files-stored-), a datacenter in
    Amsterdam, Netherlands. File versioning is disabled as Litestream files [are immutable and don't require
    versioning](https://litestream.io/guides/backblaze/#create-a-bucket).

    The files in the bucket can be listed via `b2 ls --recursive rdb-nocodb`.

[^4]: This bucket is also hosted in the EU-Central region.

    To allow access for requests issued directly by web browsers (i.e. client-side), suitable [CORS
    rules](https://www.backblaze.com/docs/cloud-storage-cross-origin-resource-sharing-rules) must be defined. An example rule granting access from the
    `https://admin.rdb.vote` origin to the [S3-compatible API](https://www.backblaze.com/docs/cloud-storage-s3-compatible-api) is found in
    [`Backblaze_B2_CORS_rule.json`](Backblaze_B2_CORS_rules.json) and can be applied using
    [`b2 bucket update --cors-rules "$(<./Backblaze_B2_CORS_rules.json)" rdb-attachments`](https://www.backblaze.com/docs/cloud-storage-enable-cors-with-the-cli).
    To inspect the currently configured rules, run `b2 bucket get rdb-attachments`.

## Admin access

We can directly connect into the root filesystem of the `rdb` Fly app via an [SSH tunnel](https://fly.io/docs/flyctl/ssh-console/) to perform any low-level
administration tasks:

``` sh
flyctl ssh console
```

We can also fetch a copy of the NocoDB SQLite metadata database to inspect it locally[^5] using SFTP:

``` sh
flyctl sftp get /usr/app/data/noco.db /PATH/TO/LOCAL_FILE.db
```

To exit the SQLite CLI, press <kbd>Ctrl</kbd>+<kbd>D</kbd>, and then enter `exit` to terminate the SSH tunnel.

[^5]: E.g. using [DB Browser for SQLite](https://sqlitebrowser.org/) or directly on the command-line using the [SQLite CLI](https://www.sqlite.org/cli.html)
    via:

    ``` sh
    sqlite3 /PATH/TO/LOCAL_FILE.db
    ```

    It might come in handy to enable pretty formatted output in the SQLite CLI:

    ``` sqlite
    .mode columns
    .headers on
    ```

### Hints

To determine the names of all tables that contain a specific value `id1234`, run the following:

``` sh
for X in $(sqlite3 /PATH/TO/LOCAL_FILE.db .tables) ; do sqlite3 /PATH/TO/LOCAL_FILE.db "SELECT * FROM ${X};" | grep >/dev/null 'id1234' && echo ${X}; done
```

<sup>[Source](https://stackoverflow.com/a/53875499/7196903)</sup>

## Deployment

To deploy a new [NocoDB release](https://github.com/nocodb/nocodb/releases/), it's recommended to adhere to the following procedure:

1.  If the update is not critical[^6], wait at least 10 days (in case of a major version update) or 5 days (in case of a minor version update[^7]) after the
    release date to avoid running into accidental regressions. If another follow-up version is released within this period, start waiting over again.

    Regardless of this cautionary waiting period, it is strongly recommended to first test new NocoDB versions on our [testing instance](#testing-instance).

2.  Change the version number under `build.image` in [`fly.toml`](fly.toml) to the one matching the new release.

3.  Create a local backup of NocoDB's SQLite database and deploy the new app version:

    ``` sh
    flyctl sftp get /usr/app/data/noco.db ".backup/$(date --iso-8601=seconds).noco.db" \
      && flyctl sftp get /usr/app/data/noco.db-wal ".backup/$(date --iso-8601=seconds).noco.db-wal" \
      && flyctl deploy --ha=false
    ```

4.  Log into [admin.rdb.vote](https://admin.rdb.vote/) and quickly verify that NocoDB basically works.

[^6]: I.e. it does not fix a critical bug in the currently deployed version.

[^7]: Beware that also minor version updates regularly introduce critical regressions, e.g. [here](https://github.com/nocodb/nocodb/issues/6328),
    [here](https://github.com/nocodb/nocodb/issues/9057) or [here](https://github.com/nocodb/nocodb/issues/9098).

## Testing instance

There's a separate NocoDB instance for testing accessible under [**`admin-testing.rdb.vote`**](https://admin-testing.rdb.vote/). It connects to the databases'
`testing` branch, so our production database remains unaffected by it. It provides a "playground" where NocoDB configuration changes as well as NocoDB updates
can be tried out without risk of breaking something.

To reflect the latest state of the database, the `testing` branch must be manually reset to it's parent branch via Neon's CLI:

``` sh
neonctl branches reset br-steep-morning-a21x0mxx --parent
```

## Further notes

-   We first considered using NocoDB's built-in RESTful [data APIs](https://data-apis-v2.nocodb.com/) to offer public access to the RDB. But since NocoDB is a
    very fast growing project that does not seem to treat [security issues](https://securitylab.github.com/advisories/GHSL-2023-141_nocodb_nocodb/) with as much
    urgency as they probably should, we buried that idea and instead rely on [PostgREST](../fly_postgrest/).

-   We use a single NocoDB instance and do not use NocoDB's [worker node feature](https://github.com/nocodb/nocodb/releases/tag/0.108.0) since the main database
    is not run by NocoDB but our [Neon PostgreSQL compute instance](../README.md#dbms). Our NocoDB instance only maintains its own (SQLite) metadata database
    and we largely ignore its RESTful data APIs, so the load on this instance should be rather minimal, hence there's no need for dedicated worker nodes.
    Consequently, we do not use Redis (by leaving [`NC_REDIS_URL`](https://docs.nocodb.com/getting-started/self-hosted/environment-variables) empty).

-   A meeting with the NocoDB team (usually Naveen Rudrappa (founder) and Raju Udava (engineer)) can be booked via [this
    link](https://calendly.com/nocodb-meeting). Such a meeting offers a good oportunity to discuss development progress and specific technical questions.

## Known issues

The following issues could not yet be resolved satisfactorily, but are very well known to us:

-   It's impossible to add new to rows to table `options` via NocoDB (see the [bug report](https://github.com/nocodb/nocodb/issues/9278) for full details).
    Hence, we have to add new rows to the `options` via other PostgreSQL clients for the time being (e.g. via
    [`rdb:::pg_tbl_update()`](https://pg--rdb-rpkg-dev.netlify.app/dev/reference/pg_tbl_update)).

### Alternatives

There are other open-source software choices comparable to NocoDB which we (as of 2024-04-22) consider less suitable for our purposes. However, as this could
change in the future, it makes sense to list them here. The more promising ones are ***emphasized***:

#### Spreadsheet front-ends for relational databases

The following software pursues the same or very similar goals as NocoDB: To provide a web application that offers an intuitive front-end for a relational
database such as PostgreSQL.

-   [APITable](https://github.com/apitable/apitable)
    -   core is [written](https://github.com/apitable/apitable) in Java
    -   seems very powerful and polished, but also appears to be an inflexible monolith since it also acts as the data store itself
    -   does not support external DBs as data store; feature request for PostgreSQL support [was closed as *not
        planned*](https://github.com/apitable/apitable/issues/604#issuecomment-2265129144)
-   [Baserow](https://baserow.io/)
    -   core is [written](https://gitlab.com/baserow/baserow) in Python
    -   has true real-time collaboration which NocoDB still lacks
    -   seems solid but is [open core](https://en.wikipedia.org/wiki/Open-core_model), which introduces considerable vendor lock-in for the [premium
        features](https://baserow.io/pricing)
    -   only supports PostgreSQL as backend (fine with us) but **misses the ability to connect to existing databases** like NocoDB does, so currently choosing
        Baserow implies full technological lock-in and no flexibility (see also [this forum
        thread](https://community.baserow.io/t/using-postgresql-and-baserow/2170))
-   [Basetool](https://www.basetool.io/)
    -   core is [written](https://github.com/basetool-io/basetool) in JavaScript/TypeScript
    -   unmaintained (last commit from [2022-06-20](https://github.com/basetool-io/basetool/commits/main/))
    -   way less feature-rich than NocoDB
-   [DbGate](https://dbgate.org/)
    -   core is [written](https://github.com/dbgate/dbgate) in JavaScript/TypeScript
    -   way less intuitive and feature-rich than NocoDB
    -   ugly UI
-   [Grist](https://www.getgrist.com/)
    -   core is [written](https://github.com/gristlabs/grist-core) in JavaScript/TypeScript
    -   only supports SQLite as backend
    -   overall less feature-rich and intuitive than NocoDB
-   [Irelia](https://ireliatable.github.io/irelia-web/)
    -   core is [written](https://github.com/IreliaTable/irelia/) in JavaScript/TypeScript
    -   only supports SQLite as backend
    -   unmaintained? (last commit from [2023-03-03](https://github.com/IreliaTable/irelia/commits/main/))
-   [***Mathesar***](https://mathesar.org/)
    -   core is [written](https://github.com/mathesar-foundation/mathesar) in Python
    -   only supports PostgreSQL as backend (fine with us)
    -   still in alpha state and not as feature-rich as NocoDB yet, but is [backed by a dedicated *Mathesar
        Foundation*](https://mathesar.org/blog/2024/03/28/mathesar-foundation-announcement.html) and the project [mission](https://mathesar.org/about.html) and
        [roadmap](https://mathesar.org/roadmap.html) seem very promising!
    -   a fair comparison with NocoDB is found [in the FAQ](https://mathesar.org/faq.html#how-is-mathesar-different-from-nocodb)
-   [Motor Admin](https://www.getmotoradmin.com/)
    -   core is [written](https://github.com/motor-admin/motor-admin) in Ruby on Rails
    -   way less feature-rich than NocoDB
-   [NocoBase](https://www.nocobase.com/)
    -   core is [written](https://github.com/nocobase/nocobase) in JavaScript/TypeScript
    -   overall less feature-rich and intuitive than NocoDB
-   [Rowy](https://www.rowy.io/)
    -   core is [written](https://github.com/rowyio/rowy) in TypeScript
    -   so far only supports [Cloud Firestore](https://firebase.google.com/docs/firestore) as backend; feature request for PostgreSQL support is found
        [here](https://github.com/rowyio/rowy/issues/228)
    -   has a beautiful UI, but is still way less feature-rich than NocoDB
-   [***Teable***](https://teable.io/)
    -   core is [written](https://github.com/teableio/teable) in JavaScript/TypeScript
    -   so far only supports PostgreSQL as backend (fine with us)
    -   has a beautiful UI and some powerful features which NocoDB still lacks like true [real-time
        collaboration](https://help.teable.io/#real-time-collaboration) or undo/redo support
    -   still **misses the ability to connect to existing databases** like NocoDB does, so currently choosing Teable implies full technological lock-in and no
        flexibility; but once support for external databases [is implemented](https://github.com/teableio/teable/issues/556), we should have a closer look
    -   misses a bunch of other features that NocoDB already offers
-   [undb](https://undb.io/)
    -   core is [written](https://github.com/undb-io/undb) in TypeScript
    -   looks [promising](https://docs.undb.io/), but [misses a bunch of features](https://undb.io/#future-features) that NocoDB already offers
    -   [only supports SQLite/Turso](https://github.com/undb-io/undb/blob/62b32adfa17e8f9543938f68ec5deed21f1db1f6/packages/env/src/index.ts#L101) as backend so
        far

#### Low-code frameworks

The following software frameworks provide "low-code" building blocks to implement customized web frontends, i.a. for relational databases. Compared to the
ready-made spreadsheet front-ends above, below frameworks would require a non-negligible additional effort to implement all the functionality we need. On the
other hand, they would allow to tailor the implementation exactly to our needs.

-   [Appsmith](https://www.appsmith.com/)
    -   core is [written](https://github.com/appsmithorg/appsmith) in Java
    -   supports [databases incl. PostgreSQL](https://www.appsmith.com/integration/postgresql)
-   [Budibase](https://budibase.com/)
    -   core is [written](https://github.com/Budibase/budibase) in JavaScript/TypeScript
    -   supports [databases incl. PostgreSQL](https://docs.budibase.com/docs/postgresql)
-   [ToolJet](https://www.tooljet.com/)
    -   core is [written](https://github.com/ToolJet/ToolJet) in JavaScript/TypeScript
    -   very powerful and supports [databases](https://www.tooljet.com/database)
-   [***Windmill***](https://www.windmill.dev/)
    -   core is [written](https://github.com/windmill-labs/windmill) in Rust ✨
    -   very elegant and driven by highly-competent [architectural decisions](https://www.windmill.dev/docs/core_concepts)
    -   provides a solid basis to implement [tailor-made web frontends](https://www.windmill.dev/docs/apps/app_editor) for [almost
        anything](https://www.windmill.dev/docs/intro) including
        [databases](https://www.windmill.dev/docs/core_concepts/persistent_storage/structured_databases)

#### Other

The *presentation of relational data*, i.e. the development of an intuitive user interface for relational databases, is an endeavour in such high demand that
it's reasonable to assume **universal standards** for *how* exactly to present *what* will emerge in the medium term.

An example of such a development is the open-source [Drizzle Studio](https://orm.drizzle.team/drizzle-studio/overview), which is e.g. [integrated in the Neon
Console](https://neon.tech/docs/changelog/2024-05-24#drizzle-studio-in-the-neon-console). More information about Drizzle Studio is found in [this GitHub
repository](https://github.com/drizzle-team/drizzle-studio-npm#readme), a live demo is accessible [here](https://demo.drizzle.studio/). In principle, NocoDB and
similar tools could be based upon Drizzle Studio (or similar lower-level software) and share the underlying engineering effort, i.e. avoid duplicating this
work. We should keep an eye on such developments.

----------------------------------------------------------------------------------------------------------------------------------------------------------------
