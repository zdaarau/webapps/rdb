# RDB backup

Custom Fly app to perform automated backups once a week.

It currently

-   saves the whole `rdb` PostgreSQL database's `main` branch to [Backblaze B2](https://www.backblaze.com/cloud-storage)[^1] and [Cloudflare
    R2](https://developers.cloudflare.com/r2/)[^2] object storage services, so we have redundant weekly database snapshots.

-   mirrors the state of the whole `rdb-attachments` bucket on Backblaze B2 to Cloudflare R2, so we get (up to 7 days delayed) redundancy in case of disaster at
    Backblaze B2.

This repository contains the non-sensitive app configuration. Sensitive configuration data is stored via [Fly secrets](https://fly.io/docs/reference/secrets/).
Currently this includes the following [environment variables](https://en.wikipedia.org/wiki/Environment_variable):

-   `PGPASSWORD`
-   `RCLONE_CONFIG_PASS`

[^1]: Backblaze B2 is an [Amazon S3 compatible](https://en.wikipedia.org/wiki/Amazon_S3#S3_API_and_competing_services) object storage service that is
    [considerably cheaper](https://www.backblaze.com/cloud-storage/pricing), but nevertheless provides excellent
    [durability](https://www.backblaze.com/docs/en/cloud-storage-resiliency-durability-and-availability).

[^2]: Cloudflare R2 is an Amazon S3 [compatible](https://developers.cloudflare.com/r2/api/s3/api/) object storage service that is [considerably
    cheaper](https://developers.cloudflare.com/r2/pricing) (although still 2.5x more expensive than Backblaze B2), but nevertheless provides excellent
    [durability](https://developers.cloudflare.com/r2/reference/durability/).

## Hosting details

The Fly [app](https://fly.io/docs/reference/apps/) is named `rdb-backup` and runs on a single [`shared-cpu-1x` instance with *256 MB*
RAM](https://fly.io/docs/about/pricing/#compute) hosted in the [*Paris, France* region](https://fly.io/docs/reference/regions/). Should it prove necessary to
expand performance, we could [increase RAM](https://fly.io/docs/flyctl/scale-memory/) and/or [switch to a faster CPU](https://fly.io/docs/flyctl/scale-vm/)
anytime[^3].

The data is backed up to the private Backblaze B2 bucket `rdb-backup`[^4] as well as the private Cloudflare R2 bucket `rdb-backup`[^5]. Hence there's no need to
attach a [persistent storage volume](https://fly.io/docs/reference/volumes/) to the Fly app. The backed up data is fully encrypted using
[*age*](https://github.com/FiloSottile/age#readme), whose strong public-key cryptography ensures neither Backblaze/Cloudflare nor any eavesdropper can read the
data. Consequently, there is no point in enabling
[Backblaze's](https://www.backblaze.com/docs/cloud-storage-server-side-encryption#serverside-encryption-options) or
[Cloudflare's](https://developers.cloudflare.com/r2/reference/data-security/) server-side encryption (which would still involve blindly trusting these
companies).

The app is set to run [once a week](https://fly.io/docs/machines/run/#start-a-machine-on-a-schedule)[^6].

[^3]: Note that scaling automatically restarts the app. The most relevant documentation on (auto)scaling Fly apps includes:

    -   [Scale Machine CPU and RAM](https://fly.io/docs/apps/scale-machine/)
    -   [Scale the Number of Machines](https://fly.io/docs/apps/scale-count/)
    -   [Automatically Stop and Start Machines](https://fly.io/docs/apps/autostart-stop/)

[^4]: The bucket is hosted in the [EU-Central region](https://help.backblaze.com/hc/en-us/articles/217664578-Where-are-my-files-stored-), a datacenter in
    Amsterdam, Netherlands. File versioning is disabled as each database snapshot has a distinct filename.

    The files in the bucket can be listed via `b2 ls --recursive b2://rdb-backup/`.

[^5]: The bucket is hosted in Cloudflare's [EU](https://developers.cloudflare.com/r2/reference/data-location/#available-jurisdictions) datacenters. There is no
    file versioning (which we wouldn't need anyways as each database snapshot has a distinct filename).

    The files in the bucket can be listed via `rclone ls r2bkp:rdb-backup`.

[^6]: Via `flyctl machine update --schedule weekly`. To display the Fly machine configuration including the currently set schedule, run
    `flyctl machine status --display-config`.

## Admin access

We can directly connect into the root filesystem of the *running* `rdb-backup` Fly app via an [SSH tunnel](https://fly.io/docs/flyctl/ssh-console/) to perform
any low-level administration tasks. Since the app is configured to immediately stop after having performed its tasks, we first have to temporarily alter the
Dockerfile to make it run indefinitely. To do so, add [e.g. the following](https://stackoverflow.com/a/42873832/7196903) at the end of
[`entrypoint.sh`](entrypoint.sh):

``` sh
sleep infinity
```

After the modified app is deployed, we can connect into it using:

``` sh
flyctl ssh console
```

Enter `exit` to terminate the SSH tunnel again.

## Further notes

-   The `rdb-attachments` bucket mirroring is done via `rclone copy`, not `rclone sync` so we do not replicate (possibly unintended) deletions – otherwise we'd
    reduce the backup strategy to absurdity. Unfortunately, this also means that unnecessary backups of rightly deleted files accumulate over time, so we need
    to manually run `rclone sync b2attch:rdb-attachments r2attch:rdb-attachments` from time to time (like once a year) to save on storage space.

-   We must set [`no_check_bucket = true`](https://rclone.org/s3/#cloudflare-r2) for Cloudflare R2 in `rclone.conf` if an API token is used that doesn't have
    `Admin` rights (i.e. permission to create buckets), which we do.

----------------------------------------------------------------------------------------------------------------------------------------------------------------
