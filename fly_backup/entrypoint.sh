#!/bin/sh

# create DB dump
pg_dump --clean --if-exists --format=custom --compress=zstd:9 --no-password \
  | age --encrypt --recipient="${AGE_PUBLIC_KEY}" --output=rdb.dump.age

# upload DB dump to object stores
BACKUP_FILE_NAME="rdb_$(date -uI).dump.age"
rclone copyto rdb.dump.age "b2bkp:rdb-backup/${BACKUP_FILE_NAME}"
rclone copyto rdb.dump.age "r2bkp:rdb-backup/${BACKUP_FILE_NAME}"

# back up referendum attachments from main object store to another
rclone copy b2attch:rdb-attachments r2attch:rdb-attachments
